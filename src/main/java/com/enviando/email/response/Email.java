package com.enviando.email.response;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.SimpleEmail;

public class Email {

    public void enviarEmail(){

        ModelAuth auth = new ModelAuth();

        SimpleEmail email = new SimpleEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(auth.getMyEmail(), auth.getPassword()));
        email.setSSLOnConnect(true);

        try {
            email.setFrom(auth.getMyEmail());
            email.setSubject("Pedido Realizado");
            email.setMsg("Pedido confirmado com succeso, aguarde os próximos passos!");
            email.addTo(auth.getMyEmail());
            email.send();
            System.out.println("Email enviado com sucesso!");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
