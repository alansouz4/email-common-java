package com.enviando.email;

import com.enviando.email.response.Email;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmailApplication.class, args);

		Email email = new Email();
		email.enviarEmail();
	}

}
